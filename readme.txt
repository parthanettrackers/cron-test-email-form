=== SAR Cron Email System Test ===
Contributors: w3developing, LLC
Tags: email, smtp, phpmailer, sendmail
Requires at least: 5
Tested up to: 5.0.4
Stable tag: 14
Requires PHP: 5.6

Here is a short description of the plugin. This should be no more than 150 characters. No markup here.

"SARS Cron Email System Test" plugin for WordPress.

== Description ==

This Plugin sends a cron-based email utilizing the SARS email Plugin test system.

= Features =           

* Test your SARS Wordpress email system
* Set a timed schedule using cPanel CRON

= Requirements =

* PHP 5.6 or higher.
* WordPress 5.4 or higher.
* Set CRON in cPanel

It may work with older versions of PHP and WordPress, but we don't support anything older than the versions mentioned above.

= Usage =

Just install in your WordPress like any other plugin, activate it and fill the simple form for testing email.


== Installation ==

* This is custom pluging. Extract the initail zip file and just drop the contents in the <code>wp-content/plugins/</code> directory of your WordPress installation (or install it directly from your dashboard) and then activate it from Plugins page.


                                                                                                                        
